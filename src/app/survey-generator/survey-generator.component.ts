import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { ResizedEvent } from 'angular-resize-event';

interface Audio {
  name: string,
  url: string,
  size: number
}
interface Question {
  prompt: string,
  label: string,
  questionAudioFile: Audio,
  responses: Response[]
}

interface Response {
  response: string,
  skipQuestion?: number
}

interface SurveyData {
  name: string,
  description: string,
  questionsCount: number,
  responseGoal: number,
  startDate: string,
  endDate: string,
  created: string,
  trunkID: string,
  surveySkipLogic: boolean,
  amd: boolean,
  timeout: number,
  channels: number,
  volume: number,
  surveyID: string,
  responseTrackingQuestion: number,
  introAudioFile: Audio,
  hangupAudioFile: Audio,
  questions: Question[]
}

const keys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#']

@Component({
  selector: 'survey-generator',
  templateUrl: './survey-generator.component.html',
  styleUrls: ['./survey-generator.component.scss']
})
export class SurveyGeneratorComponent implements OnInit {
  generated: boolean = false
  surveyInitiator: FormGroup
  basicDetails: FormGroup
  questionFormsGroup: FormGroup
  audios__orig: Audio[] = []
  audios: Audio[] = []
  keys = keys

  surveyData: SurveyData = <SurveyData>{}


  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {

    // Survey initiator form
    this.surveyInitiator = this.fb.group({
      name: new FormControl(),
      description: new FormControl(),
      questionsCount: new FormControl(1),
      responseGoal: new FormControl(25),
      startDate: new FormControl(new Date()),
      endDate: new FormControl(new Date()),
      created: new FormControl({ value: new Date(), disabled: true }),
      trunkID: new FormControl('None'),
      surveySkipLogic: new FormControl(),
      amd: new FormControl(),
      timeout: new FormControl(),
      channels: new FormControl(),
      volume: new FormControl()
    })

    // Basic details form
    this.basicDetails = this.fb.group({
      surveyID: new FormControl({ value: 'some random auto-generated ID', disabled: true }),
      responseTrackingQuestion: new FormControl(),
      introAudioFile: new FormControl({ value: '', disabled: true }, Validators.required),
      hangupAudioFile: new FormControl({ value: 'None', disabled: true })
    })
    this.questionFormsGroup = this.fb.group({
      questionFormsArray: this.fb.array([])
    })

    this.addQuestion()

    this.surveyInitiator.get('questionsCount').valueChanges.subscribe((value) => {
      this.updateQuestionForms(value)
    })
  }


  generatePreview() {
    this.surveyData.questions = [];

    (this.questionFormsGroup.get('questionFormsArray') as FormArray).controls.forEach((question: FormControl | FormArray | FormGroup) => {
      let responses: Response[] = [];

      (question.get('responses') as FormArray).controls.forEach(response => {
        if (response.get('dmtf').value)
          responses.push({
            response: response.get('response').value,
            skipQuestion: response.get('skipQuestion').value
          })
      })

      this.surveyData.questions.push(<Question>{
        prompt: question.get('prompt').value,
        label: question.get('label').value,
        questionAudioFile: this.audios.find(audio => audio.name == question.get('questionAudioFile').value),
        responses: responses
      })
    })

    Object.keys(this.surveyInitiator.controls).forEach((key: string) => {
      this.surveyData[key] = this.surveyInitiator.controls[key].value
    })
    Object.keys(this.basicDetails.controls).forEach((key: string) => {
      this.surveyData[key] = this.basicDetails.controls[key].value
    })
  }


  onResized(event: ResizedEvent) {
    console.log(event.newWidth)
  }

  updateQuestionForms(questionsCount: number) {
    let questionsCount__old = (this.questionFormsGroup.get('questionFormsArray') as FormArray).length

    if (questionsCount == questionsCount__old) return

    for (let i = 0; i < Math.abs(questionsCount - questionsCount__old); i++) {
      questionsCount > questionsCount__old
        ? this.addQuestion()
        : this.removeQuestion()
    }
  }

  updateQuestionsCount(mode: string) {
    let curQuestionsCount = this.surveyInitiator.get('questionsCount').value

    if (curQuestionsCount == 1 && mode == 'remove') return

    this.surveyInitiator.get('questionsCount').setValue(
      curQuestionsCount + (
        mode == 'add'
          ? 1
          : -1)
    )

  }

  addQuestion() {
    let responses = new FormArray([])

    keys.forEach(key => {
      (responses as FormArray).push(this.fb.group({
        dmtf: new FormControl(),
        response: new FormControl({ value: '', disabled: true }),
        skipQuestion: new FormControl({ value: '', disabled: true })
      }))
    });

    (this.questionFormsGroup.get('questionFormsArray') as FormArray).push(
      this.fb.group({
        prompt: new FormControl(),
        label: new FormControl(),
        questionAudioFile: new FormControl(),
        responses: responses
      })
    )
  }

  removeQuestion() {
    (this.questionFormsGroup.get('questionFormsArray') as FormArray).removeAt(-1)
  }

  updateResponseGoal(mode: string) {
    let curResponseGoal = this.surveyInitiator.get('responseGoal').value

    if (curResponseGoal == 1 && mode == 'remove') return

    this.surveyInitiator.get('responseGoal').setValue(
      curResponseGoal + (
        mode == 'add'
          ? 25
          : -25
      )
    )
  }

  formatTimeout(value: number | null): string {
    if (!value)
      return 'None'

    let mins = Math.floor(value / 60)
    let secs = value - (mins * 60)

    let formatted = ''

    if (mins) {
      formatted = `${mins} minute${mins > 1 ? 's' : ''}`
      if (secs)
        formatted += ` and ${secs} second${secs > 1 ? 's' : ''}`
    } else
      formatted = `${secs} second${secs > 1 ? 's' : ''}`

    let rings = Math.floor(value / 6)
    formatted += ` (${rings} ring${rings == 1 ? '' : 's'})`

    return formatted
  }

  timeoutSliderLabel(value: number | null) {
    if (!value) return 'None'

    let mins = Math.floor(value / 60)
    let secs = value - (mins * 60)

    return `${mins ? mins + 'm' : ''}${secs ? secs + 's' : ''}`
  }

  onUploadError(event) {
    console.log(event)
  }

  onUploadSuccess(audioFiles: File[]) {
    let oldSize = this.audios__orig.length

    const newSize = audioFiles.length

    while (oldSize < newSize) {
      let audio: Audio = <Audio>{
        name: audioFiles[oldSize].name,
        size: audioFiles[oldSize].size,
      }

      const reader = new FileReader()

      reader.onload = (e: ProgressEvent) => {
        audio.url = (e.target as FileReader).result.toString()
      }
      reader.readAsDataURL(audioFiles[oldSize])

      this.audios.push(audio)
      this.audios__orig.push(audio)

      oldSize++

    }

    this.setAudioSelect()
  }

  removeAudioFile(name: string) {
    this.audios.splice(this.audios.indexOf(this.audios.find(audio => audio.name == name)), 1)

    this.setAudioSelect()
  }

  setAudioSelect() {
    if (this.audios.length) {
      if (this.basicDetails.get('introAudioFile').disabled)
        this.basicDetails.get('introAudioFile').enable()

      if (this.basicDetails.get('hangupAudioFile').disabled)
        this.basicDetails.get('hangupAudioFile').enable()

    } else {
      if (this.basicDetails.get('introAudioFile').enabled)
        this.basicDetails.get('introAudioFile').disable()

      if (this.basicDetails.get('hangupAudioFile').enabled)
        this.basicDetails.get('hangupAudioFile').disable()
    }
  }

  responseStatus(questionID: number, responseID: number) {
    return ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('dmtf').value
  }

  skipQuestionStatus(questionID: number, responseID: number) {
    return (
      ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('skipQuestion').enabled
      && this.surveyInitiator.get('surveySkipLogic').value
      && ((this.questionFormsGroup.get('questionFormsArray') as FormArray).length - 1 != questionID)
    )
  }

  toggleDMTF(mode: boolean, questionID: number, responseID: number) {
    if (mode) {
      ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('response').enable();
      ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('skipQuestion').enable();

    } else {
      ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('response').disable();

      ((this.questionFormsGroup.get('questionFormsArray') as FormArray).at(questionID).get('responses') as FormArray).at(responseID).get('skipQuestion').disable();

    }
  }

  parseKey(i: number): string {
    return keys[i]
  }
}
